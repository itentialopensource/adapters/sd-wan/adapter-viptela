# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Viptela System. The API that was used to build the adapter for Viptela is usually available in the report directory of this adapter. The adapter utilizes the Viptela API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco Viptela adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco’s Viptela controller to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. Viptela fits within the overall Cisco Digital Network Architecture (Cisco DNA) to complement its platform for WAN optimization, automation, virtualization and analytics.

With this adapter you have the ability to perform operations with Cisco Viptela such as:

- Configure and Manage Cisco Devices in the Cloud. 
- Add and Remove Devices to Inventory: Through IAP, a new device can be added to the network inventory so that it can be managed by Ansible Manger. A device that is no longer utilized from the network inventory can also be removed through IAP.
- Perform Pre and Post Checks: IAP allows for the ability to perform pre and post checks of a device configuration when making modifications to the device.
- Device Configuration Assurance: IAP can back up the configuration on the device in a native format that can be restored to the device if there are issues.
- Template
- Policy
- Alarm

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
