## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Viptela. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Viptela.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Viptela. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterface(query, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatisticsinterface(body, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfaceaggregation(query, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatisticsinterfaceaggregation(body, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfacecsv(query, callback)</td>
    <td style="padding:15px">Get  raw data as CSV</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfacedoccount(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatisticsinterfacedoccount(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfacefields(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfacepage(query, scrollId, count, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatisticsinterfacepage(body, scrollId, count, callback)</td>
    <td style="padding:15px">Get  raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsinterfacequeryfields(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/statistics/interface/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatadevicestatestateDataType(stateDataType, startId, count, callback)</td>
    <td style="padding:15px">Retrieve device state data</td>
    <td style="padding:15px">{base_path}/{version}/data/device/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatadevicestatestateDataTypefields(stateDataType, callback)</td>
    <td style="padding:15px">Retrieve device state data fileds</td>
    <td style="padding:15px">{base_path}/{version}/data/device/state/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDatadevicestatestateDataTypequery(stateDataType, callback)</td>
    <td style="padding:15px">Retrieve device state data fileds</td>
    <td style="padding:15px">{base_path}/{version}/data/device/state/{pathv1}/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(callback)</td>
    <td style="padding:15px">Retrieve list of all devices</td>
    <td style="padding:15px">{base_path}/{version}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceconfig(deviceId, callback)</td>
    <td style="padding:15px">Retrieve device running configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceconfightml(deviceId, callback)</td>
    <td style="padding:15px">Retrieve device running configuration in HTML</td>
    <td style="padding:15px">{base_path}/{version}/device/config/html?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicecounters(callback)</td>
    <td style="padding:15px">Retrieve device counters</td>
    <td style="padding:15px">{base_path}/{version}/device/counters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicedevicestatus(callback)</td>
    <td style="padding:15px">Retrieve device status</td>
    <td style="padding:15px">{base_path}/{version}/device/devicestatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicehardwarehealthdetail(callback)</td>
    <td style="padding:15px">Retrieve detailed hardware health</td>
    <td style="padding:15px">{base_path}/{version}/device/hardwarehealth/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicehardwarehealthsummary(isCached, callback)</td>
    <td style="padding:15px">Retrieve hardware health summary</td>
    <td style="padding:15px">{base_path}/{version}/device/hardwarehealth/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicekeyvalue(callback)</td>
    <td style="padding:15px">Retrieve device list as key/value pairs</td>
    <td style="padding:15px">{base_path}/{version}/device/keyvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicemodels(list, callback)</td>
    <td style="padding:15px">Retrieve all device models</td>
    <td style="padding:15px">{base_path}/{version}/device/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicemodelsuuid(uuid, callback)</td>
    <td style="padding:15px">Retrieve the device model for the device</td>
    <td style="padding:15px">{base_path}/{version}/device/models/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicemonitor(callback)</td>
    <td style="padding:15px">Retrieve all device monitoring details</td>
    <td style="padding:15px">{base_path}/{version}/device/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicequeues(callback)</td>
    <td style="padding:15px">Retrieve synchronized queue information</td>
    <td style="padding:15px">{base_path}/{version}/device/queues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicereachable(callback)</td>
    <td style="padding:15px">Retrieve list of reachable devices</td>
    <td style="padding:15px">{base_path}/{version}/device/reachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicestats(callback)</td>
    <td style="padding:15px">Retrieve synchronized queue information</td>
    <td style="padding:15px">{base_path}/{version}/device/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicestatus(callback)</td>
    <td style="padding:15px">Retrieve device status</td>
    <td style="padding:15px">{base_path}/{version}/device/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesyncStatus(groupId, callback)</td>
    <td style="padding:15px">Retrieve list of currently syncing devices</td>
    <td style="padding:15px">{base_path}/{version}/device/sync_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesyncallmemorydb(callback)</td>
    <td style="padding:15px">Synchronize memory database for all devices</td>
    <td style="padding:15px">{base_path}/{version}/device/syncall/memorydb?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicetloc(callback)</td>
    <td style="padding:15px">Retrieve TLOC status</td>
    <td style="padding:15px">{base_path}/{version}/device/tloc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicetlocutil(callback)</td>
    <td style="padding:15px">Retrieve TLOC list</td>
    <td style="padding:15px">{base_path}/{version}/device/tlocutil?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicetlocutildetail(callback)</td>
    <td style="padding:15px">Retrieve detailed TLOC list</td>
    <td style="padding:15px">{base_path}/{version}/device/tlocutil/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceunreachable(callback)</td>
    <td style="padding:15px">Retrieve list of unreachable devices</td>
    <td style="padding:15px">{base_path}/{version}/device/unreachable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceunreachabledeviceIP(deviceIP, callback)</td>
    <td style="padding:15px">Delete unreachable device</td>
    <td style="padding:15px">{base_path}/{version}/device/unreachable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicevedgeinventorydetail(callback)</td>
    <td style="padding:15px">Retrieve detailed vEdge inventory</td>
    <td style="padding:15px">{base_path}/{version}/device/vedgeinventory/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicevedgeinventorysummary(callback)</td>
    <td style="padding:15px">Retrieve vEdge inventory</td>
    <td style="padding:15px">{base_path}/{version}/device/vedgeinventory/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicevmanage(callback)</td>
    <td style="padding:15px">Retrieve vManage configuration</td>
    <td style="padding:15px">{base_path}/{version}/device/vmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionchangepartition(deviceId, callback)</td>
    <td style="padding:15px">Retrieve change partition information</td>
    <td style="padding:15px">{base_path}/{version}/device/action/changepartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionchangepartition(body, callback)</td>
    <td style="padding:15px">Process change partition operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/changepartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactiondefaultpartition(body, callback)</td>
    <td style="padding:15px">Process marking default partition operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/defaultpartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionfiltervpn(callback)</td>
    <td style="padding:15px">Create VPN list</td>
    <td style="padding:15px">{base_path}/{version}/device/action/filter/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactioninstall(deviceId, callback)</td>
    <td style="padding:15px">Generate install info</td>
    <td style="padding:15px">{base_path}/{version}/device/action/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactioninstall(body, callback)</td>
    <td style="padding:15px">Process an installation operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactioninstalldevicesdeviceType(deviceType, groupId, callback)</td>
    <td style="padding:15px">Retrieve list of installed devices</td>
    <td style="padding:15px">{base_path}/{version}/device/action/install/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionlist(callback)</td>
    <td style="padding:15px">Retrieve list of device actions</td>
    <td style="padding:15px">{base_path}/{version}/device/action/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionreboot(deviceId, callback)</td>
    <td style="padding:15px">Retrieve reboot information</td>
    <td style="padding:15px">{base_path}/{version}/device/action/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionreboot(body, callback)</td>
    <td style="padding:15px">Process a reboot operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionrebootdevicesdeviceType(deviceType, groupId, callback)</td>
    <td style="padding:15px">Retrieve list of rebooted devices</td>
    <td style="padding:15px">{base_path}/{version}/device/action/reboot/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionrediscover(callback)</td>
    <td style="padding:15px">Retrieve rediscover operation information</td>
    <td style="padding:15px">{base_path}/{version}/device/action/rediscover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionrediscover(body, callback)</td>
    <td style="padding:15px">Rediscover device</td>
    <td style="padding:15px">{base_path}/{version}/device/action/rediscover?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionrediscoverall(callback)</td>
    <td style="padding:15px">Rediscover device</td>
    <td style="padding:15px">{base_path}/{version}/device/action/rediscoverall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionremovepartition(deviceId, callback)</td>
    <td style="padding:15px">Retrieve remove partition information</td>
    <td style="padding:15px">{base_path}/{version}/device/action/removepartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionremovepartition(body, callback)</td>
    <td style="padding:15px">Process remove partition operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/removepartition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionstartmonitor(callback)</td>
    <td style="padding:15px">Triggers global monitoring thread</td>
    <td style="padding:15px">{base_path}/{version}/device/action/startmonitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionuniquevpnlist(body, callback)</td>
    <td style="padding:15px">Create VPN list</td>
    <td style="padding:15px">{base_path}/{version}/device/action/uniquevpnlist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionvpn(callback)</td>
    <td style="padding:15px">Create VPN list</td>
    <td style="padding:15px">{base_path}/{version}/device/action/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionztpupgrade(body, callback)</td>
    <td style="padding:15px">Process ZTP upgrade configuration.</td>
    <td style="padding:15px">{base_path}/{version}/device/action/ztp/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionztpupgrade(body, callback)</td>
    <td style="padding:15px">Process ZTP upgrade configuration.</td>
    <td style="padding:15px">{base_path}/{version}/device/action/ztp/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceactionztpupgradesetting(body, callback)</td>
    <td style="padding:15px">Process ZTP upgrade configuration.</td>
    <td style="padding:15px">{base_path}/{version}/device/action/ztp/upgrade/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceactionztpupgradesetting(body, callback)</td>
    <td style="padding:15px">Process ZTP upgrade configuration.</td>
    <td style="padding:15px">{base_path}/{version}/device/action/ztp/upgrade/setting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateVsmartPolicy(policyId, body, callback)</td>
    <td style="padding:15px">Activate a vSmart controller policy by policy ID</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/activate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateVsmartPolicy(policyId, callback)</td>
    <td style="padding:15px">Deactivate a vSmart controller policy by policy ID</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/deactivate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceVsmartStatus(policyId, callback)</td>
    <td style="padding:15px">Display the status of device vSmart policy</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVsmartPolicyList(callback)</td>
    <td style="padding:15px">Display the vSmart policy list</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDeviceTemplateInput(body, callback)</td>
    <td style="padding:15px">Get raw data</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewDeviceTemplateConfiguration(body, callback)</td>
    <td style="padding:15px">Get raw data</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachDeviceTemplateCli(body, callback)</td>
    <td style="padding:15px">Get raw data</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachcli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachDeviceTemplateFeature(body, callback)</td>
    <td style="padding:15px">Get raw data</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachfeature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachDeviceTemplateCli(body, callback)</td>
    <td style="padding:15px">Get raw data</td>
    <td style="padding:15px">{base_path}/{version}/template/config/device/mode/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceList(masterTemplateId, callback)</td>
    <td style="padding:15px">getDeviceList</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/available/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushMixTemplate(body, callback)</td>
    <td style="padding:15px">pushMixTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedConfigToDevice(deviceId, callback)</td>
    <td style="padding:15px">getAttachedConfigToDevice</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachedconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushMasterTemplate(body, callback)</td>
    <td style="padding:15px">pushMasterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachfeature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedDeviceList(masterTemplateId, callback)</td>
    <td style="padding:15px">getAttachedDeviceList</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attached/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInputWithoutDevice(body, callback)</td>
    <td style="padding:15px">createInputWithoutDevice</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/exportcsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfigurationPreview(body, callback)</td>
    <td style="padding:15px">getDeviceConfigurationPreview</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processInputCommaSepFile(callback)</td>
    <td style="padding:15px">processInputCommaSepFile</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/process/input/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateTemplate(body, callback)</td>
    <td style="padding:15px">validateTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushCloudxConfig(body, callback)</td>
    <td style="padding:15px">pushCloudxConfig</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachcloudx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCloudxConfig(body, callback)</td>
    <td style="padding:15px">editCloudxConfig</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachcloudx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachSites(body, callback)</td>
    <td style="padding:15px">detachSites</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/detachcloudx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesWithDuplicateIP(body, callback)</td>
    <td style="padding:15px">getDevicesWithDuplicateIP</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/duplicateip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pushCLITemplate(body, callback)</td>
    <td style="padding:15px">pushCLITemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/attachcli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachDeviceTemplate(body, callback)</td>
    <td style="padding:15px">detachDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/detach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceInput(body, callback)</td>
    <td style="padding:15px">createDeviceInput</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkVbond(callback)</td>
    <td style="padding:15px">checkVbond</td>
    <td style="padding:15px">{base_path}/{version}/template/device/config/vbond?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateTemplateList(summary, callback)</td>
    <td style="padding:15px">generateTemplateList</td>
    <td style="padding:15px">{base_path}/{version}/template/feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplate(body, callback)</td>
    <td style="padding:15px">createTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplate(templateId, callback)</td>
    <td style="padding:15px">getTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplate(templateId, body, callback)</td>
    <td style="padding:15px">editTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplate(templateId, callback)</td>
    <td style="padding:15px">deleteTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editLITemplate(templateId, body, callback)</td>
    <td style="padding:15px">editLITemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/li/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLITemplate(body, callback)</td>
    <td style="padding:15px">createLITemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/li?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLITemplate(callback)</td>
    <td style="padding:15px">listLITemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/li?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateTemplateByDeviceType(deviceType, callback)</td>
    <td style="padding:15px">generateTemplateByDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateDefinition(templateId, callback)</td>
    <td style="padding:15px">getTemplateDefinition</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/definition/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateTemplateTypes(callback)</td>
    <td style="padding:15px">generateTemplateTypes</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateTemplateTypeDefinition(typeName, version, callback)</td>
    <td style="padding:15px">generateTemplateTypeDefinition</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/types/definition/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateMasterTemplateDefinition(typeName, callback)</td>
    <td style="padding:15px">generateMasterTemplateDefinition</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/master/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplatesAttachedToFeature(templateId, callback)</td>
    <td style="padding:15px">getDeviceTemplatesAttachedToFeature</td>
    <td style="padding:15px">{base_path}/{version}/template/feature/devicetemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateMasterTemplateList(feature, callback)</td>
    <td style="padding:15px">generateMasterTemplateList</td>
    <td style="padding:15px">{base_path}/{version}/template/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editMasterTemplate(templateId, body, callback)</td>
    <td style="padding:15px">editMasterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVSmartTemplate(body, callback)</td>
    <td style="padding:15px">createVSmartTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateByPolicyId(policyId, callback)</td>
    <td style="padding:15px">getTemplateByPolicyId</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/definition/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVSmartTemplate(policyId, body, callback)</td>
    <td style="padding:15px">editVSmartTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVSmartTemplate(policyId, callback)</td>
    <td style="padding:15px">deleteVSmartTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetVedgeCloud(uuid, callback)</td>
    <td style="padding:15px">resetVedgeCloud</td>
    <td style="padding:15px">{base_path}/{version}/system/device/reset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateBootstrapConfigForVedges(body, callback)</td>
    <td style="padding:15px">generateBootstrapConfigForVedges</td>
    <td style="padding:15px">{base_path}/{version}/system/device/bootstrap/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decommissionVedgeCloud(uuid, callback)</td>
    <td style="padding:15px">decommissionVedgeCloud</td>
    <td style="padding:15px">{base_path}/{version}/system/device/decommission/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateBootstrapConfigForVedge(uuid, configtype = 'cloudinit', callback)</td>
    <td style="padding:15px">generateBootstrapConfigForVedge</td>
    <td style="padding:15px">{base_path}/{version}/system/device/bootstrap/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBootstrapConfigZip(id, callback)</td>
    <td style="padding:15px">getBootstrapConfigZip</td>
    <td style="padding:15px">{base_path}/{version}/system/device/bootstrap/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">formPost(callback)</td>
    <td style="padding:15px">formPost</td>
    <td style="padding:15px">{base_path}/{version}/system/device/fileupload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesDetails(deviceCategory = 'vedges', model, state = 'tokengenerated', uuid, deviceIP, validity = 'valid', callback)</td>
    <td style="padding:15px">getDevicesDetails</td>
    <td style="padding:15px">{base_path}/{version}/system/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(body, callback)</td>
    <td style="padding:15px">createDevice</td>
    <td style="padding:15px">{base_path}/{version}/system/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagementSystemIPInfo(callback)</td>
    <td style="padding:15px">getManagementSystemIPInfo</td>
    <td style="padding:15px">{base_path}/{version}/system/device/management/systemip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(uuid, callback)</td>
    <td style="padding:15px">deleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/system/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDevice(uuid, body, callback)</td>
    <td style="padding:15px">editDevice</td>
    <td style="padding:15px">{base_path}/{version}/system/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerVEdgeSyncStatus(callback)</td>
    <td style="padding:15px">getControllerVEdgeSyncStatus</td>
    <td style="padding:15px">{base_path}/{version}/system/device/controllers/vedge/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootCertStatusAll(state = 'pending', callback)</td>
    <td style="padding:15px">getRootCertStatusAll</td>
    <td style="padding:15px">{base_path}/{version}/system/device/rootcertchain/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncDevices(body, callback)</td>
    <td style="padding:15px">syncDevices</td>
    <td style="padding:15px">{base_path}/{version}/system/device/smartaccount/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudDockDataBasedOnDeviceType(deviceCategory = 'vedges', callback)</td>
    <td style="padding:15px">getCloudDockDataBasedOnDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/system/device/type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOutOfSyncTemplates(callback)</td>
    <td style="padding:15px">getOutOfSyncTemplates</td>
    <td style="padding:15px">{base_path}/{version}/template/device/syncstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOutOfSyncDevices(templateId, callback)</td>
    <td style="padding:15px">getOutOfSyncDevices</td>
    <td style="padding:15px">{base_path}/{version}/template/device/syncstatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMasterTemplate(body, callback)</td>
    <td style="padding:15px">createMasterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCLITemplate(body, callback)</td>
    <td style="padding:15px">createCLITemplate</td>
    <td style="padding:15px">{base_path}/{version}/template/device/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMasterTemplateDefinition(templateId, callback)</td>
    <td style="padding:15px">getMasterTemplateDefinition</td>
    <td style="padding:15px">{base_path}/{version}/template/device/object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRunningConfig(deviceUUID, callback)</td>
    <td style="padding:15px">getRunningConfig</td>
    <td style="padding:15px">{base_path}/{version}/template/config/running/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadConfig(deviceUUID, body, callback)</td>
    <td style="padding:15px">uploadConfig</td>
    <td style="padding:15px">{base_path}/{version}/template/config/attach/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachedConfig(deviceUUID, type, callback)</td>
    <td style="padding:15px">getAttachedConfig</td>
    <td style="padding:15px">{base_path}/{version}/template/config/attached/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigDiff(deviceUUID, callback)</td>
    <td style="padding:15px">getConfigDiff</td>
    <td style="padding:15px">{base_path}/{version}/template/config/diff/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCLIModeDevices(type = 'vedge', callback)</td>
    <td style="padding:15px">generateCLIModeDevices</td>
    <td style="padding:15px">{base_path}/{version}/template/config/device/mode/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceToCLIMode(body, callback)</td>
    <td style="padding:15px">updateDeviceToCLIMode</td>
    <td style="padding:15px">{base_path}/{version}/template/config/device/mode/cli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generatevManageModeDevices(type = 'vedge', callback)</td>
    <td style="padding:15px">generatevManageModeDevices</td>
    <td style="padding:15px">{base_path}/{version}/template/config/device/mode/vmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCompatibleDevices(oldDeviceUUID, callback)</td>
    <td style="padding:15px">getCompatibleDevices</td>
    <td style="padding:15px">{base_path}/{version}/template/config/rmalist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnForDevice(uuid, callback)</td>
    <td style="padding:15px">getVpnForDevice</td>
    <td style="padding:15px">{base_path}/{version}/template/config/vpn/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rmaUpdate(body, callback)</td>
    <td style="padding:15px">rmaUpdate</td>
    <td style="padding:15px">{base_path}/{version}/template/config/rmaupdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deActivatePolicy(policyId, callback)</td>
    <td style="padding:15px">deActivatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/deactivate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activatePolicy(policyId, body, callback)</td>
    <td style="padding:15px">activatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/activate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkVSmartConnectivityStatus(callback)</td>
    <td style="padding:15px">checkVSmartConnectivityStatus</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vsmart/connectivity/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationAwareRoutingStatistics(body, callback)</td>
    <td style="padding:15px">applicationAwareRoutingStatistics</td>
    <td style="padding:15px">{base_path}/{version}/statistics/approute/fec/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getColoGroups(callback)</td>
    <td style="padding:15px">Get COLO groups</td>
    <td style="padding:15px">{base_path}/{version}/admin/cologroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createColoGroup(body, callback)</td>
    <td style="padding:15px">Add COLO group</td>
    <td style="padding:15px">{base_path}/{version}/admin/cologroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editColoGroup(id, body, callback)</td>
    <td style="padding:15px">Update COLO group</td>
    <td style="padding:15px">{base_path}/{version}/admin/cologroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteColoGroup(id, callback)</td>
    <td style="padding:15px">Delete COLO group</td>
    <td style="padding:15px">{base_path}/{version}/admin/cologroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUsers(callback)</td>
    <td style="padding:15px">Get all users</td>
    <td style="padding:15px">{base_path}/{version}/admin/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create a user</td>
    <td style="padding:15px">{base_path}/{version}/admin/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveSessions(callback)</td>
    <td style="padding:15px">Get active sessions</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/activeSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validatePassword(body, callback)</td>
    <td style="padding:15px">Validate user password</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/password/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePassword(userName, body, callback)</td>
    <td style="padding:15px">Update user password</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/password/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProfilePassword(body, callback)</td>
    <td style="padding:15px">Update profile password</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/profile/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeSessions(body, callback)</td>
    <td style="padding:15px">Remove sessions</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/removeSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetUser(body, callback)</td>
    <td style="padding:15px">Unlock a user</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUserRole(callback)</td>
    <td style="padding:15px">Check whether a user has admin role</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUserAuthType(callback)</td>
    <td style="padding:15px">Find user authentication type, whether it is SAML enabled</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/userAuthType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(userName, body, callback)</td>
    <td style="padding:15px">Update user</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(userName, callback)</td>
    <td style="padding:15px">Delete user</td>
    <td style="padding:15px">{base_path}/{version}/admin/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUserGroups(callback)</td>
    <td style="padding:15px">Get all user groups</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserGroup(body, callback)</td>
    <td style="padding:15px">Create user group</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroupGridColumns(callback)</td>
    <td style="padding:15px">Get user groups in a grid table</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup/definition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUserGroupsAsKeyValue(callback)</td>
    <td style="padding:15px">Get user groups as key value map</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup/keyvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserGroup(userGroupId, body, callback)</td>
    <td style="padding:15px">Update user group</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserGroup(userGroupId, callback)</td>
    <td style="padding:15px">Delete user group</td>
    <td style="padding:15px">{base_path}/{version}/admin/usergroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnGroups(callback)</td>
    <td style="padding:15px">Get VPN groups</td>
    <td style="padding:15px">{base_path}/{version}/admin/vpngroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnGroup(body, callback)</td>
    <td style="padding:15px">Add VPN group</td>
    <td style="padding:15px">{base_path}/{version}/admin/vpngroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVpnGroup(id, body, callback)</td>
    <td style="padding:15px">Update VPN group</td>
    <td style="padding:15px">{base_path}/{version}/admin/vpngroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnGroup(id, callback)</td>
    <td style="padding:15px">Delete VPN group</td>
    <td style="padding:15px">{base_path}/{version}/admin/vpngroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarms(query, callback)</td>
    <td style="padding:15px">Get alarms for last 30min if vManage query is not specified</td>
    <td style="padding:15px">{base_path}/{version}/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRawAlarmData(body, callback)</td>
    <td style="padding:15px">Gets lists of alarms along with the raw alarm data of each.</td>
    <td style="padding:15px">{base_path}/{version}/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmAggregationData(query, callback)</td>
    <td style="padding:15px">Gets aggregated list of alarms along with the raw alarm data of each aggregation</td>
    <td style="padding:15px">{base_path}/{version}/alarms/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostAlarmAggregationData(body, callback)</td>
    <td style="padding:15px">Gets aggregated list of alarms along with the raw alarm data of each aggregation</td>
    <td style="padding:15px">{base_path}/{version}/alarms/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearStaleAlarm(body, callback)</td>
    <td style="padding:15px">Clears specific stale alarm</td>
    <td style="padding:15px">{base_path}/{version}/alarms/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNonViewedActiveAlarmsCount(callback)</td>
    <td style="padding:15px">Get count of the alarms which are active and acknowledged by the user</td>
    <td style="padding:15px">{base_path}/{version}/alarms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDisabledAlarm(callback)</td>
    <td style="padding:15px">List all disabled alarms</td>
    <td style="padding:15px">{base_path}/{version}/alarms/disabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableEnableAlarm(eventName, disable, time, body, callback)</td>
    <td style="padding:15px">Enable/Disable a specific alarm</td>
    <td style="padding:15px">{base_path}/{version}/alarms/disabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCount1(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/alarms/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountPost1(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/alarms/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFields1(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/alarms/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markAllAlarmsAsViewed(type, body, callback)</td>
    <td style="padding:15px">Mark all larms as acknowledged by the user</td>
    <td style="padding:15px">{base_path}/{version}/alarms/markallasviewed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">markAlarmsAsViewed(body, callback)</td>
    <td style="padding:15px">Mark alarms as acknowledged by the user</td>
    <td style="padding:15px">{base_path}/{version}/alarms/markviewed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNonViewedAlarms(callback)</td>
    <td style="padding:15px">Get alarms which are active and acknowledged by the user</td>
    <td style="padding:15px">{base_path}/{version}/alarms/notviewed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatBulkAlarmRawData(query, scrollId, count, callback)</td>
    <td style="padding:15px">Get paginated alarm raw data</td>
    <td style="padding:15px">{base_path}/{version}/alarms/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatBulkAlarmRawData(scrollId, count, body, callback)</td>
    <td style="padding:15px">Get paginated alarm raw data</td>
    <td style="padding:15px">{base_path}/{version}/alarms/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPeriodicPurgeTimer(interval, activeTime, callback)</td>
    <td style="padding:15px">Set alarm purge timer</td>
    <td style="padding:15px">{base_path}/{version}/alarms/purgefrequency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatQueryFields1(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/alarms/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlarmQueryConfig(callback)</td>
    <td style="padding:15px">Get query configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarms/query/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">correlAntiEntropy(callback)</td>
    <td style="padding:15px">Reset correlation engine data</td>
    <td style="padding:15px">{base_path}/{version}/alarms/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartCorrelationEngine(callback)</td>
    <td style="padding:15px">Restart correlation engine</td>
    <td style="padding:15px">{base_path}/{version}/alarms/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmTypesAsKeyValue(callback)</td>
    <td style="padding:15px">Gets alarm type as key value pair</td>
    <td style="padding:15px">{base_path}/{version}/alarms/rulenamedisplay/keyvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmsBySeverity(severityLevel = 'major', deviceId, query, callback)</td>
    <td style="padding:15px">Get alarm by severity</td>
    <td style="padding:15px">{base_path}/{version}/alarms/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSeverityCustomHistogram(query, callback)</td>
    <td style="padding:15px">Get alarm severity histogram</td>
    <td style="padding:15px">{base_path}/{version}/alarms/severity/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSeverityMappings(callback)</td>
    <td style="padding:15px">Gets alarm severity mappings</td>
    <td style="padding:15px">{base_path}/{version}/alarms/severitymappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTracking(testName, callback)</td>
    <td style="padding:15px">Start tracking events</td>
    <td style="padding:15px">{base_path}/{version}/alarms/starttracking/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStats(callback)</td>
    <td style="padding:15px">Get alarm statistics</td>
    <td style="padding:15px">{base_path}/{version}/alarms/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopTracking(testName, callback)</td>
    <td style="padding:15px">Stop tracking events</td>
    <td style="padding:15px">{base_path}/{version}/alarms/stoptracking/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmDetails(alarmUuid, callback)</td>
    <td style="padding:15px">Get alarm detail</td>
    <td style="padding:15px">{base_path}/{version}/alarms/uuid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotificationRule(ruleId, body, callback)</td>
    <td style="padding:15px">Update notification rule</td>
    <td style="padding:15px">{base_path}/{version}/notifications/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationRule(body, callback)</td>
    <td style="padding:15px">Add notification rule</td>
    <td style="padding:15px">{base_path}/{version}/notifications/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationRule(ruleId, callback)</td>
    <td style="padding:15px">Get all rules or specific notification rule by its Id</td>
    <td style="padding:15px">{base_path}/{version}/notifications/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationRule(ruleId, callback)</td>
    <td style="padding:15px">Delete notification rule</td>
    <td style="padding:15px">{base_path}/{version}/notifications/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawAuditLogData(inputQuery, callback)</td>
    <td style="padding:15px">Get stat raw data</td>
    <td style="padding:15px">{base_path}/{version}/auditlog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRawPropertyData(body, callback)</td>
    <td style="padding:15px">Get raw property data with post action</td>
    <td style="padding:15px">{base_path}/{version}/auditlog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPropertyAggregationData(inputQuery, callback)</td>
    <td style="padding:15px">Get raw property data aggregated</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostPropertyAggregationData(body, callback)</td>
    <td style="padding:15px">Get raw property data aggregated with post action</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditCount(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditCountPost(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFields(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatBulkRawPropertyData(inputQuery, scrollId, count, callback)</td>
    <td style="padding:15px">Get raw property data in bulk</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatBulkRawPropertyData(scrollId, count, body, callback)</td>
    <td style="padding:15px">Get raw property data in bulk with post action</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatQueryFields(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAuditLog(query, callback)</td>
    <td style="padding:15px">Get audit logs for last 3 hours</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditSeverityCustomHistogram(query, callback)</td>
    <td style="padding:15px">Get audit log severity histogram</td>
    <td style="padding:15px">{base_path}/{version}/auditlog/severity/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchduledBackup(taskId, backupInfoId, callback)</td>
    <td style="padding:15px">Delete all or a specific backup file stored in vManage</td>
    <td style="padding:15px">{base_path}/{version}/backup/backupinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalBackupInfo(localBackupInfoId, callback)</td>
    <td style="padding:15px">Get a localBackupInfo record by localBackupInfoId</td>
    <td style="padding:15px">{base_path}/{version}/backup/backupinfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadBackupFile(pathParam, callback)</td>
    <td style="padding:15px">Download a Backup File that is already stored in vManage</td>
    <td style="padding:15px">{base_path}/{version}/backup/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBackup(size, callback)</td>
    <td style="padding:15px">List all backup files of a tenant stored in vManage</td>
    <td style="padding:15px">{base_path}/{version}/backup/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importScheduledBackup(callback)</td>
    <td style="padding:15px">Submit a previously backed up file and import the data and apply it to the configuraion database</td>
    <td style="padding:15px">{base_path}/{version}/restore/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteImportBackup(body, callback)</td>
    <td style="padding:15px">Remote import backup from a remote URL and import the data and apply it to the configuraion database</td>
    <td style="padding:15px">{base_path}/{version}/restore/remoteimport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSchedules(limit, callback)</td>
    <td style="padding:15px">Get a schedule record for backup by scheduler id</td>
    <td style="padding:15px">{base_path}/{version}/schedule/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleRecordForBackup(schedulerId, callback)</td>
    <td style="padding:15px">Get a schedule record for backup by scheduler id</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedule(schedulerId, callback)</td>
    <td style="padding:15px">Delete a schedule record for backup in vManage by scheduler id</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportBackup(body, callback)</td>
    <td style="padding:15px">Trigger a backup of configuration database and statstics database and store it in vManage</td>
    <td style="padding:15px">{base_path}/{version}/backup/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertDetails(body, callback)</td>
    <td style="padding:15px">Get cert details</td>
    <td style="padding:15px">{base_path}/{version}/certificate/certdetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCSRViewRightMenus(callback)</td>
    <td style="padding:15px">Get CSR detail view</td>
    <td style="padding:15px">{base_path}/{version}/certificate/csr/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceViewRightMenus(callback)</td>
    <td style="padding:15px">Get device detail view</td>
    <td style="padding:15px">{base_path}/{version}/certificate/device/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesList(callback)</td>
    <td style="padding:15px">Get vEdge list</td>
    <td style="padding:15px">{base_path}/{version}/certificate/device/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceSyncRootCert(body, callback)</td>
    <td style="padding:15px">Force sync root certificate   Note: In a multitenant vManage system, this API is only available in</td>
    <td style="padding:15px">{base_path}/{version}/certificate/forcesync/rootCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCSR(body, callback)</td>
    <td style="padding:15px">Generate CSR   Note: In a multitenant vManage system, this API is only available in the Provider an</td>
    <td style="padding:15px">{base_path}/{version}/certificate/generate/csr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateEnterpriseCSR(body, callback)</td>
    <td style="padding:15px">Generate CSR   Note: In a multitenant vManage system, this API is only available in the Provider an</td>
    <td style="padding:15px">{base_path}/{version}/certificate/generate/enterprise/csr/vedge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installCertificate(body, callback)</td>
    <td style="padding:15px">Install singed certificate   Note: In a multitenant vManage system, this API is only available in t</td>
    <td style="padding:15px">{base_path}/{version}/certificate/install/signedCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListStatus(callback)</td>
    <td style="padding:15px">get certificate</td>
    <td style="padding:15px">{base_path}/{version}/certificate/list/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateData(callback)</td>
    <td style="padding:15px">Get certificate chain</td>
    <td style="padding:15px">{base_path}/{version}/certificate/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetRSA(body, callback)</td>
    <td style="padding:15px">Register CSR   Note: In a multitenant vManage system, this API is only available in the Provider an</td>
    <td style="padding:15px">{base_path}/{version}/certificate/reset/rsa?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decommissionEnterpriseCSRForVedge(body, callback)</td>
    <td style="padding:15px">Revoking enterprise CSR for hardware vEdge   Note: In a multitenant vManage system, this API is onl</td>
    <td style="padding:15px">{base_path}/{version}/certificate/revoke/enterprise/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootCertChains(action = 'get', callback)</td>
    <td style="padding:15px">Get root cert chain</td>
    <td style="padding:15px">{base_path}/{version}/certificate/rootcertchains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveRootCertChain(body, callback)</td>
    <td style="padding:15px">Save root cert chain   Note: In a multitenant vManage system, this API is only available in the Pro</td>
    <td style="padding:15px">{base_path}/{version}/certificate/rootcertchains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootCertificate(callback)</td>
    <td style="padding:15px">Get device root certificate detail view</td>
    <td style="padding:15px">{base_path}/{version}/certificate/rootcertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveVEdgeList(body, callback)</td>
    <td style="padding:15px">Save vEdge device list</td>
    <td style="padding:15px">{base_path}/{version}/certificate/save/vedge/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateDetail(callback)</td>
    <td style="padding:15px">Get certificate detail</td>
    <td style="padding:15px">{base_path}/{version}/certificate/stats/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateStats(callback)</td>
    <td style="padding:15px">Get certificate expiration status</td>
    <td style="padding:15px">{base_path}/{version}/certificate/stats/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncvBond(callback)</td>
    <td style="padding:15px">sync vManage UUID to all vBond</td>
    <td style="padding:15px">{base_path}/{version}/certificate/syncvbond?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvEdgeList(model = 'vedge-cloud', state, callback)</td>
    <td style="padding:15px">Get vEdge list</td>
    <td style="padding:15px">{base_path}/{version}/certificate/vedge/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setvEdgeList(body, callback)</td>
    <td style="padding:15px">Save vEdge list</td>
    <td style="padding:15px">{base_path}/{version}/certificate/vedge/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getView(callback)</td>
    <td style="padding:15px">Get certificate UI view</td>
    <td style="padding:15px">{base_path}/{version}/certificate/view?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvSmartList(callback)</td>
    <td style="padding:15px">Get vSmart list   Note: In a multitenant vManage system, this API is only available in the Provider</td>
    <td style="padding:15px">{base_path}/{version}/certificate/vsmart/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setvSmartList(callback)</td>
    <td style="padding:15px">Save vSmart list</td>
    <td style="padding:15px">{base_path}/{version}/certificate/vsmart/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfiguration(uuid, replaceController, deviceId, callback)</td>
    <td style="padding:15px">Invalidate device   Note: In a multitenant vManage system, this API is only available in the Provid</td>
    <td style="padding:15px">{base_path}/{version}/certificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSelfSignedCert(callback)</td>
    <td style="padding:15px">get self signed certificate</td>
    <td style="padding:15px">{base_path}/{version}/certificate/vmanage/selfsignedcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProxyCertOfEdge(deviceId, callback)</td>
    <td style="padding:15px">Get edge proxy certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCertificate(body, callback)</td>
    <td style="padding:15px">Upload device certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addWANEdge(deviceId, body, callback)</td>
    <td style="padding:15px">Add SSL proxy wan edge</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/certificate/wanedge/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCertificiates(bodyFormData, callback)</td>
    <td style="padding:15px">Upload device certificates</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslProxyCSR(deviceId, callback)</td>
    <td style="padding:15px">Get SSL proxy CSR</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/csr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceCertificates(body, callback)</td>
    <td style="padding:15px">Get certificate for all cEdges</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/devicecertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceCSR(body, callback)</td>
    <td style="padding:15px">Get CSR for all cEdges</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/devicecsr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateSslProxyCSR(body, callback)</td>
    <td style="padding:15px">CSR request SSL proxy for edge</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/generate/csr/sslproxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateSSLProxyCSR(body, callback)</td>
    <td style="padding:15px">Generate CSR</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/generate/vmanage/csr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslProxyList(callback)</td>
    <td style="padding:15px">Get SSL proxy certificate list</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewCertificate(body, callback)</td>
    <td style="padding:15px">Renew device certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeCertificate(body, callback)</td>
    <td style="padding:15px">Revoke device certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeRenewCertificate(body, callback)</td>
    <td style="padding:15px">Revoke and renew device certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/revokerenew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateState(callback)</td>
    <td style="padding:15px">Get certificate state</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEnterpriseCertificate(callback)</td>
    <td style="padding:15px">Get enterprise certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/enterprise/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setEnterpriseCert(body, callback)</td>
    <td style="padding:15px">Configure enterprise certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/enterprise/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVManageEnterpriseRootCertificate(callback)</td>
    <td style="padding:15px">Get vManage enterprise root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/enterprise/rootca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setEnterpriseRootCaCert(body, callback)</td>
    <td style="padding:15px">Set vManage enterprise root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/enterprise/rootca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvManageCertificate(callback)</td>
    <td style="padding:15px">Get vManage intermediate certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/vmanage/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setvManageintermediateCert(body, callback)</td>
    <td style="padding:15px">Set vManage root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/vmanage/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvManageCSR(callback)</td>
    <td style="padding:15px">Get vManage CSR</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/vmanage/csr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvManageRootCA(callback)</td>
    <td style="padding:15px">Get vManage root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/vmanage/rootca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setvManageRootCA(body, callback)</td>
    <td style="padding:15px">Set vManage root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslproxy/settings/vmanage/rootca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessTokenforDevice(callback)</td>
    <td style="padding:15px">getAccessTokenforDevice</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/accesstoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAzureToken(body, callback)</td>
    <td style="padding:15px">Get Azure token</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/authtoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">systemconnect(callback)</td>
    <td style="padding:15px">Telemetry Opt In</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudCredentials(callback)</td>
    <td style="padding:15px">Get cloud service credentials</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCloudCredentials(body, callback)</td>
    <td style="padding:15px">Get cloud service settings</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCode(callback)</td>
    <td style="padding:15px">Get Azure device code</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/devicecode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isStaging(callback)</td>
    <td style="padding:15px">Check if testbed or production</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/staging?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelemetryState(callback)</td>
    <td style="padding:15px">Get Telemetry state</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/telemetry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">optIn(body, callback)</td>
    <td style="padding:15px">Telemetry Opt In</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/telemetry/optin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">optOut(body, callback)</td>
    <td style="padding:15px">Telemetry Opt Out</td>
    <td style="padding:15px">{base_path}/{version}/cloudservices/telemetry/optout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudSettings(callback)</td>
    <td style="padding:15px">Get cloud service settings</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOTP(callback)</td>
    <td style="padding:15px">Get cloud service OTP value</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/otp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatetOTP(body, callback)</td>
    <td style="padding:15px">Update cloud service OTP value</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/otp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEntityOwnershipInfo(callback)</td>
    <td style="padding:15px">List all entity ownership info</td>
    <td style="padding:15px">{base_path}/{version}/entityownership/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">entityOwnershipInfo(callback)</td>
    <td style="padding:15px">Entity ownership info grouped by buckets</td>
    <td style="padding:15px">{base_path}/{version}/entityownership/tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkIfClusterLocked(callback)</td>
    <td style="padding:15px">Check whether cluster is locked   Note: In a multitenant vManage system, this API is only available</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/clusterLocked?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureVmanage(body, callback)</td>
    <td style="padding:15px">Configure vManage   Note: In a multitenant vManage system, this API is only available in the Provid</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/configure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectedDevices(vmanageIP, callback)</td>
    <td style="padding:15px">Get connected device for vManage   Note: In a multitenant vManage system, this API is only availabl</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/connectedDevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSDAVC(callback)</td>
    <td style="padding:15px">Enable SDAVC   Note: In a multitenant vManage system, this API is only available in the Provider vi</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/enableSDAVC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthDetails(callback)</td>
    <td style="padding:15px">Get cluster health check details   Note: In a multitenant vManage system, this API is only availabl</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/health/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthStatusInfo(callback)</td>
    <td style="padding:15px">Get cluster health check details   Note: In a multitenant vManage system, this API is only availabl</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/health/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthSummary(isCached, callback)</td>
    <td style="padding:15px">Get cluster health check summary   Note: In a multitenant vManage system, this API is only availabl</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/health/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguredIPList(vmanageID, callback)</td>
    <td style="padding:15px">Get configured IP addresses   Note: In a multitenant vManage system, this API is only available in</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/iplist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isClusterReady(callback)</td>
    <td style="padding:15px">Is cluster ready   Note: In a multitenant vManage system, this API is only available in the Provide</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/isready?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVmanages(callback)</td>
    <td style="padding:15px">List vManages in the cluster   Note: In a multitenant vManage system, this API is only available in</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nodeProperties(callback)</td>
    <td style="padding:15px">Get properties of vManage being added to  cluster   Note: In a multitenant vManage system, this API</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/nodeProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeVmanage(body, callback)</td>
    <td style="padding:15px">Remove vManage from cluster   Note: In a multitenant vManage system, this API is only available in</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">performReplicationAndRebalanceOfKafkaPartitions(callback)</td>
    <td style="padding:15px">Initiate replication and rebalance of kafka topics   Note: In a multitenant vManage system, this AP</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/replicateAndRebalance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetSDAVCNode(body, callback)</td>
    <td style="padding:15px">Reset SDAVC on the vManage node   Note: In a multitenant vManage system, this API is only available</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/resetSDAVC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editVmanage(body, callback)</td>
    <td style="padding:15px">Update vManage cluster info   Note: In a multitenant vManage system, this API is only available in</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVmanage(body, callback)</td>
    <td style="padding:15px">Add vManage to cluster</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/setup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyMode(callback)</td>
    <td style="padding:15px">Get vManage tenancy mode   Note: In a multitenant vManage system, this API is only available in the</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/tenancy/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTenancyMode(body, callback)</td>
    <td style="padding:15px">Update vManage tenancy mode</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/tenancy/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantsList(callback)</td>
    <td style="padding:15px">Get tenant list   Note: In a multitenant vManage system, this API is only available in the Provider</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/tenantList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReplicationFactorForKafkaTopics(callback)</td>
    <td style="padding:15px">Update replication factor for each kakfa topic   Note: In a multitenant vManage system, this API is</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/updateReplicationFactor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrUpdateUserCredentials(body, callback)</td>
    <td style="padding:15px">Add or update user credentials for cluster operations   Note: In a multitenant vManage system, this</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/userCreds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVManageDetails(vmanageIP, callback)</td>
    <td style="padding:15px">Get vManage detail   Note: In a multitenant vManage system, this API is only available in the Provi</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/vManage/details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnectedDevicesPerTenant(tenantId, vmanageIP, callback)</td>
    <td style="padding:15px">Get connected device for vManage for a tenant   Note: In a multitenant vManage system, this API is</td>
    <td style="padding:15px">{base_path}/{version}/clusterManagement/{pathv1}/connectedDevices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudDockClusterDetail(clusterName, callback)</td>
    <td style="padding:15px">Get details of all existing Clusters</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudDockCluster(body, callback)</td>
    <td style="padding:15px">Update a existing cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudDockCluster(body, callback)</td>
    <td style="padding:15px">Add a new cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acitvateCloudDockCluster(clusterName, callback)</td>
    <td style="padding:15px">Activate a cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dummyccm(clusterName, callback)</td>
    <td style="padding:15px">Activate dummp cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/activateClusterDummy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dummycspState(clusterName, state, callback)</td>
    <td style="padding:15px">Activate cluster in a state</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/activateClusterDummyState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCspToCluster(body, callback)</td>
    <td style="padding:15px">Update attached csp to cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/attached/csp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudDockClusterPreview(serialNumber, callback)</td>
    <td style="padding:15px">Clouddock cluster preview</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deAcitvateCloudDockCluster(clusterId, callback)</td>
    <td style="padding:15px">Deactivate clouddock cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudDockClusterDetailById(clusterId, callback)</td>
    <td style="padding:15px">Get cluster by Id</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudDockClusterByName(clustername, callback)</td>
    <td style="padding:15px">Delete cluster by name</td>
    <td style="padding:15px">{base_path}/{version}/colocation/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterDetailsByClusterId(clusterId, callback)</td>
    <td style="padding:15px">Provide details of ids of existing clusters</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterConfigByClusterId(clusterId, callback)</td>
    <td style="padding:15px">Provide details of devices of clusters</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/cluster/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterPortMappingByClusterId(clusterId, callback)</td>
    <td style="padding:15px">Provide details of port mappings in the cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/cluster/portView?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceDetailByDeviceId(deviceId, callback)</td>
    <td style="padding:15px">List details for Device</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemStatusByDeviceId(deviceId, callback)</td>
    <td style="padding:15px">List all connected VNF to a device</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/device/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvnfByDeviceId(deviceId, callback)</td>
    <td style="padding:15px">List all VNF attached with Device</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/device/vnf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkFunctionMap(callback)</td>
    <td style="padding:15px">Retrieve network function listing</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/networkfunction/listmap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpnfDetails(clusterId, callback)</td>
    <td style="padding:15px">List all PNF by cluster Id</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/pnf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPNFConfig(pnfSerialNumber, clusterId, callback)</td>
    <td style="padding:15px">List configuration of PNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/pnf/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceChainDetails(clusterId, userGroupName, callback)</td>
    <td style="padding:15px">List all service chain or service chains by Id</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/servicechain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroupByClusterId(clusterId, callback)</td>
    <td style="padding:15px">List all attached serviceGroups to cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/servicegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getvnfDetails(clusterId, userGroupName, callback)</td>
    <td style="padding:15px">Provide details of all existing VNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vnfActions(vmName, action, callback)</td>
    <td style="padding:15px">VNF action</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVNFEventsCountDetail(userGroup, callback)</td>
    <td style="padding:15px">Get event detail of VNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVNFAlarmCount(userGroup, callback)</td>
    <td style="padding:15px">Get event detail of VNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf/alarms/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVNFEventsDetail(vnfName, callback)</td>
    <td style="padding:15px">Get event detail of VNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVNFInterfaceDetail(vnfName, deviceIp, deviceClass, callback)</td>
    <td style="padding:15px">Get interface detail of VNF</td>
    <td style="padding:15px">{base_path}/{version}/colocation/monitor/vnf/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachServiceChain(body, callback)</td>
    <td style="padding:15px">Attach service chain to cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicechain/attach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachServiceChain(body, callback)</td>
    <td style="padding:15px">Detach service chain</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicechain/detach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEdgeDevices(callback)</td>
    <td style="padding:15px">Get edge devices</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicechain/edge/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpnfDevices(pnfDeviceType, callback)</td>
    <td style="padding:15px">Get PNF edge devices</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicechain/edge/pnfdevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceChain(serviceGroupName, callback)</td>
    <td style="padding:15px">Get service chain by name</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceGroupCluster(body, callback)</td>
    <td style="padding:15px">Update service group</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceGroupCluster(body, callback)</td>
    <td style="padding:15px">Add new service group</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceGroupInCluster(clusterId, userGroupName, callback)</td>
    <td style="padding:15px">Get service chains in cluster</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup/attached?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultChain(callback)</td>
    <td style="padding:15px">Get default service chains</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup/servicechain/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableChains(callback)</td>
    <td style="padding:15px">Get all service chains</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup/servicechains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceGroupCluster(name, callback)</td>
    <td style="padding:15px">Delete service group</td>
    <td style="padding:15px">{base_path}/{version}/colocation/servicegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateContainerOnRemoteHost(containerName, url, hostIp, checksum, callback)</td>
    <td style="padding:15px">Activate container on remote host</td>
    <td style="padding:15px">{base_path}/{version}/container-manager/activate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deActivateContainer(containerName, hostIp, callback)</td>
    <td style="padding:15px">Deactivate container on remote host</td>
    <td style="padding:15px">{base_path}/{version}/container-manager/deactivate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">doesValidImageExist(containerName, callback)</td>
    <td style="padding:15px">Get container image checksum</td>
    <td style="padding:15px">{base_path}/{version}/container-manager/doesValidImageExist/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainerInspectData(containerName, hostIp, callback)</td>
    <td style="padding:15px">Get container inspect data</td>
    <td style="padding:15px">{base_path}/{version}/container-manager/inspect/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainerSettings(containerName, hostIp, callback)</td>
    <td style="padding:15px">Get container settings</td>
    <td style="padding:15px">{base_path}/{version}/container-manager/settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChecksum(callback)</td>
    <td style="padding:15px">Get container image checksum</td>
    <td style="padding:15px">{base_path}/{version}/sdavc/checksum?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomApp(callback)</td>
    <td style="padding:15px">Displays the user-defined applications</td>
    <td style="padding:15px">{base_path}/{version}/sdavc/customapps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateContainer(taskId, body, callback)</td>
    <td style="padding:15px">Activate container</td>
    <td style="padding:15px">{base_path}/{version}/sdavc/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testLoadBalancer(callback)</td>
    <td style="padding:15px">Test SD_AVC load balancer</td>
    <td style="padding:15px">{base_path}/{version}/sdavc/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBlockSync(blockSync, callback)</td>
    <td style="padding:15px">Set collection manager block set flag</td>
    <td style="padding:15px">{base_path}/{version}/device/blockSync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSDAVCOnDevice(deviceIP, enable, callback)</td>
    <td style="padding:15px">Enable/Disable SDAVC on device</td>
    <td style="padding:15px">{base_path}/{version}/device/enableSDAVC/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsType(callback)</td>
    <td style="padding:15px">Get statistics types</td>
    <td style="padding:15px">{base_path}/{version}/data/device/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveAlarms(scrollId, startDate, endDate, count, timeZone, callback)</td>
    <td style="padding:15px">Get active alarms</td>
    <td style="padding:15px">{base_path}/{version}/data/device/statistics/alarm/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDeviceStatisticsData(stateDataType, scrollId, startDate, endDate, count, timeZone, callback)</td>
    <td style="padding:15px">Get device statistics data</td>
    <td style="padding:15px">{base_path}/{version}/data/device/statistics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountWithStateDataType(stateDataType, startDate, endDate, timeZone, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/data/device/statistics/{pathv1}/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFieldsByStateDataType(stateDataType, callback)</td>
    <td style="padding:15px">Get statistics fields and types</td>
    <td style="padding:15px">{base_path}/{version}/data/device/statistics/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStats(body, callback)</td>
    <td style="padding:15px">Get statistics data</td>
    <td style="padding:15px">{base_path}/{version}/dca/analytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllStatsDataDCA(body, callback)</td>
    <td style="padding:15px">Get all statistics setting data</td>
    <td style="padding:15px">{base_path}/{version}/dca/analytics/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessToken(callback)</td>
    <td style="padding:15px">Get DCA access token</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/accesstoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">storeAccessToken(body, callback)</td>
    <td style="padding:15px">Set DCA access token</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/accesstoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAlarm(body, callback)</td>
    <td style="padding:15px">Generate DCA alarms</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/alarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdToken(callback)</td>
    <td style="padding:15px">Get DCA Id token</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/idtoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">storeIdToken(body, callback)</td>
    <td style="padding:15px">Set DCA Id token</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/idtoken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTelemetrySettings(callback)</td>
    <td style="padding:15px">Get DCA telemetry settings</td>
    <td style="padding:15px">{base_path}/{version}/dca/cloudservices/telemetry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDCADeviceStateData(stateDataType, body, callback)</td>
    <td style="padding:15px">Get device state data</td>
    <td style="padding:15px">{base_path}/{version}/dca/data/device/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDCADeviceStatisticsData(statsDataType, body, callback)</td>
    <td style="padding:15px">Get device statistics data</td>
    <td style="padding:15px">{base_path}/{version}/dca/data/device/statistics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDCATenantOwners(callback)</td>
    <td style="padding:15px">Get DCA tenant owners</td>
    <td style="padding:15px">{base_path}/{version}/dca/dcatenantowners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllDevicesDCA(body, callback)</td>
    <td style="padding:15px">Get all devices</td>
    <td style="padding:15px">{base_path}/{version}/dca/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrashLogs(body, callback)</td>
    <td style="padding:15px">Get crash log</td>
    <td style="padding:15px">{base_path}/{version}/dca/device/crashlog/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCrashLogsSynced(deviceId, callback)</td>
    <td style="padding:15px">Get device crash log</td>
    <td style="padding:15px">{base_path}/{version}/dca/device/crashlog/synced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudServicesConfigurationDCA(callback)</td>
    <td style="padding:15px">Get DCA cloud service configuration</td>
    <td style="padding:15px">{base_path}/{version}/dca/settings/configuration/cloudservices/dca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDCAAnalyticsDataFile(type = 'analytics', body, callback)</td>
    <td style="padding:15px">Create analytics config data</td>
    <td style="padding:15px">{base_path}/{version}/dca/settings/configuration/{pathv1}/dca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsDBIndexStatus(body, callback)</td>
    <td style="padding:15px">Get statistics setting status</td>
    <td style="padding:15px">{base_path}/{version}/dca/statistics/settings/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesDetailsDCA(body, callback)</td>
    <td style="padding:15px">Get device details</td>
    <td style="padding:15px">{base_path}/{version}/dca/system/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDCAAttachedConfigToDevice(body, callback)</td>
    <td style="padding:15px">Get attached config to device</td>
    <td style="padding:15px">{base_path}/{version}/dca/template/device/config/attachedconfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatePolicyDefinitionsDCA(body, callback)</td>
    <td style="padding:15px">Get template policy definitions</td>
    <td style="padding:15px">{base_path}/{version}/dca/template/policy/definition/approute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVPNListsDCA(body, callback)</td>
    <td style="padding:15px">Get VPN details</td>
    <td style="padding:15px">{base_path}/{version}/dca/template/policy/list/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVedgeTemplateListDCA(body, callback)</td>
    <td style="padding:15px">Get vEdge template list</td>
    <td style="padding:15px">{base_path}/{version}/dca/template/policy/vedge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVsmartTemplateListDCA(body, callback)</td>
    <td style="padding:15px">Get vSmart template list</td>
    <td style="padding:15px">{base_path}/{version}/dca/template/policy/vsmart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAAAUsers(deviceId, callback)</td>
    <td style="padding:15px">Get AAA users from device (Real Time)</td>
    <td style="padding:15px">{base_path}/{version}/device/aaa/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getACLMatchCounterUsers(deviceId, callback)</td>
    <td style="padding:15px">Get ACL match counters from device (Real Time)</td>
    <td style="padding:15px">{base_path}/{version}/device/acl/matchcounter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnclaimedVedges(deviceId, callback)</td>
    <td style="padding:15px">Get unclaimed vEdges from vbond</td>
    <td style="padding:15px">{base_path}/{version}/device/unclaimed/vedges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersFromDevice(deviceId, callback)</td>
    <td style="padding:15px">Get users from device (Real Time)</td>
    <td style="padding:15px">{base_path}/{version}/device/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceUsers(deviceId, callback)</td>
    <td style="padding:15px">Get all users from device</td>
    <td style="padding:15px">{base_path}/{version}/device/users/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcActivate(body, callback)</td>
    <td style="padding:15px">Process an activation operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcDelete(body, callback)</td>
    <td style="padding:15px">Process a delete operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcdelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcInstall(body, callback)</td>
    <td style="padding:15px">Process an installation operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcinstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcReload(body, callback)</td>
    <td style="padding:15px">Process a reload operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcreload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcReset(body, callback)</td>
    <td style="padding:15px">Process a reset operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcreset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processLxcUpgrade(body, callback)</td>
    <td style="padding:15px">Process an upgrade operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/lxcupgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processDeleteAmpApiKey(uuid, callback)</td>
    <td style="padding:15px">Process amp api key deletion operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/security/amp/apikey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processAmpApiReKey(body, callback)</td>
    <td style="padding:15px">Process amp api re-key operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/security/amp/rekey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testApiKey(uuid, callback)</td>
    <td style="padding:15px">Get API key from device</td>
    <td style="padding:15px">{base_path}/{version}/device/action/security/apikey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateSecurityDevicesList(policyType = 'zoneBasedFW', groupId, callback)</td>
    <td style="padding:15px">Get list of devices by security policy type</td>
    <td style="padding:15px">{base_path}/{version}/device/action/security/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testIoxConfig(deviceIP, callback)</td>
    <td style="padding:15px">testIoxConfig</td>
    <td style="padding:15px">{base_path}/{version}/device/action/test/ioxconfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processVnfInstall(body, callback)</td>
    <td style="padding:15px">Process an installation operation</td>
    <td style="padding:15px">{base_path}/{version}/device/action/vnfinstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findSoftwareImages(callback)</td>
    <td style="padding:15px">Get software images</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImageURL(body, callback)</td>
    <td style="padding:15px">Create software image URL</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageProperties(versionId, callback)</td>
    <td style="padding:15px">Get Image Properties</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/imageProperties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findSoftwareImagesWithFilters(imageType, vnfType, callback)</td>
    <td style="padding:15px">Get software images</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPnfProperties(pnfType, callback)</td>
    <td style="padding:15px">Get PNF Properties</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/pnfproperties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findVEdgeSoftwareVersion(callback)</td>
    <td style="padding:15px">Get vEdge software version</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/vedge/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findSoftwareVersion(callback)</td>
    <td style="padding:15px">Get software version</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfProperties(versionId, callback)</td>
    <td style="padding:15px">Get VNF Properties</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/vnfproperties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findZtpSoftwareVersion(callback)</td>
    <td style="padding:15px">Get ZTP software version</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/ztp/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImageURL(versionId, body, callback)</td>
    <td style="padding:15px">Update software image URL</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImageURL(versionId, callback)</td>
    <td style="padding:15px">Delete software image URL</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installPkg(callback)</td>
    <td style="padding:15px">Install software package</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUploadImagesCount(imageType, callback)</td>
    <td style="padding:15px">Number of software image presented in vManage repository</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/imageCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadPackageFile(fileName, imageType, callback)</td>
    <td style="padding:15px">Download software package file</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processSoftwareImage(imageType, callback)</td>
    <td style="padding:15px">Install software image package</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileContents(uuid, callback)</td>
    <td style="padding:15px">Get bootstrap file contents</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/custom/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editConfigFile(uuid, body, callback)</td>
    <td style="padding:15px">Edit bootstrap file</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/custom/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadImageFile(type = 'image', callback)</td>
    <td style="padding:15px">Upload virtual image/bootstrap file</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/custom/uploads/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVnfPackage(body, callback)</td>
    <td style="padding:15px">Create VNF custom package</td>
    <td style="padding:15px">{base_path}/{version}/device/action/software/package/custom/vnfPackage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelPendingTasks(processId, callback)</td>
    <td style="padding:15px">Bulk cancel task status</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/cancel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cleanStatus(cleanStatus, callback)</td>
    <td style="padding:15px">Delete task and status vertex</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/clean?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStatus(processId, callback)</td>
    <td style="padding:15px">Delete status of action</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findRunningTasks(callback)</td>
    <td style="padding:15px">Find running tasks</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveTaskCount(callback)</td>
    <td style="padding:15px">Get active task count</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/tasks/activeCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCleanStatus(processId, callback)</td>
    <td style="padding:15px">Delete task and status vertex</td>
    <td style="padding:15px">{base_path}/{version}/device/action/status/tasks/clean?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeatureList(deviceId, callback)</td>
    <td style="padding:15px">Get feature lists from device (Real Time)</td>
    <td style="padding:15px">{base_path}/{version}/device/featurelist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyncedFeatureList(deviceId, callback)</td>
    <td style="padding:15px">Get feature lists synchronously from device</td>
    <td style="padding:15px">{base_path}/{version}/device/featurelist/synced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawData16(query, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/device/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsRawData16(body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/device/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregationDataByQuery16(query, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/device/history/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostAggregationDataByQuery15(body, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/device/history/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLastThousandConfigList(deviceId, query, callback)</td>
    <td style="padding:15px">Get device config history</td>
    <td style="padding:15px">{base_path}/{version}/device/history/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfig(configId, callback)</td>
    <td style="padding:15px">Get device config</td>
    <td style="padding:15px">{base_path}/{version}/device/history/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawDataAsCSV16(query, callback)</td>
    <td style="padding:15px">Get raw data with optional query as CSV</td>
    <td style="padding:15px">{base_path}/{version}/device/history/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCount18(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/device/history/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountPost18(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/device/history/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFields18(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/device/history/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatBulkRawData16(query, scrollId, count, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/device/history/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatBulkRawData16(scrollId, count, body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/device/history/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatQueryFields18(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/device/history/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdminTech(body, callback)</td>
    <td style="padding:15px">Generate admin tech logs</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/admintech?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadAdminTechFile(filename, callback)</td>
    <td style="padding:15px">Download admin tech logs</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/admintech/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdminTechFile(requestID, callback)</td>
    <td style="padding:15px">Delete admin tech logs</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/admintech/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdminTechs(callback)</td>
    <td style="padding:15px">Get device admin-tech information</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/admintechs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">npingDevice(deviceIP, body, callback)</td>
    <td style="padding:15px">NPing device</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/nping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingDevice(deviceIP, body, callback)</td>
    <td style="padding:15px">Ping device</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/ping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processPortHopColor(deviceIP, body, callback)</td>
    <td style="padding:15px">Request port hop color</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/porthopcolor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processInterfaceReset(deviceIP, body, callback)</td>
    <td style="padding:15px">Reset device interface</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/reset/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processResetUser(deviceIP, body, callback)</td>
    <td style="padding:15px">Request reset user</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/resetuser/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">servicePath(deviceIP, body, callback)</td>
    <td style="padding:15px">Service path</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/servicepath/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tracerouteDevice(deviceIP, body, callback)</td>
    <td style="padding:15px">Traceroute</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/traceroute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelPath(deviceIP, body, callback)</td>
    <td style="padding:15px">TunnelPath</td>
    <td style="padding:15px">{base_path}/{version}/device/tools/tunnelpath/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControlConnections(uuid, callback)</td>
    <td style="padding:15px">Troubleshoot control connections</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/control/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConfiguration(uuid, callback)</td>
    <td style="padding:15px">Debug device bring up</td>
    <td style="padding:15px">{base_path}/{version}/troubleshooting/devicebringup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDBSchema(callback)</td>
    <td style="padding:15px">Get the current database schema</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/dbschema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThreadPools(callback)</td>
    <td style="padding:15px">Get information on the threadpools</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics/threadpools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawData23(query, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsRawData23(body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregationDataByQuery23(query, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostAggregationDataByQuery22(body, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawDataAsCSV23(query, callback)</td>
    <td style="padding:15px">Get raw data with optional query as CSV</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCount25(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountPost25(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFields25(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatBulkRawData23(query, scrollId, count, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatBulkRawData23(scrollId, count, body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatQueryFields25(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/statistics/speedtest/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionInfoCapture(body, callback)</td>
    <td style="padding:15px">getSessionInfoCapture</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePacketCaptureSession(sessionId, callback)</td>
    <td style="padding:15px">disablePacketCaptureSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/disable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadFile(sessionId, callback)</td>
    <td style="padding:15px">downloadFile</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceStopPcapSession(sessionId, callback)</td>
    <td style="padding:15px">forceStopPcapSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/forcedisbale/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startPcapSession(sessionId, callback)</td>
    <td style="padding:15px">startPcapSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/start/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileDownloadStatus(sessionId, callback)</td>
    <td style="padding:15px">getFileDownloadStatus</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopPcapSession(sessionId, callback)</td>
    <td style="padding:15px">stopPcapSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/stop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">formPostPacketCapture(deviceUUID, sessionId, callback)</td>
    <td style="padding:15px">formPostPacketCapture</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/capture/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionInfoLog(body, callback)</td>
    <td style="padding:15px">getSessionInfoLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableDeviceLog(sessionId, callback)</td>
    <td style="padding:15px">disableDeviceLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/disable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadDebugLog(sessionId, callback)</td>
    <td style="padding:15px">downloadDebugLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/download/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewSessionInfo(sessionId, callback)</td>
    <td style="padding:15px">renewSessionInfo</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/renew/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchDeviceLog(sessionId, body, callback)</td>
    <td style="padding:15px">searchDeviceLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessions(callback)</td>
    <td style="padding:15px">getSessions</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearSession(sessionId, callback)</td>
    <td style="padding:15px">clearSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/sessions/clear/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogType(uuid, callback)</td>
    <td style="padding:15px">getLogType</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamLog(logType, deviceUUID, sessionId, body, callback)</td>
    <td style="padding:15px">streamLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLog(sessionId, logId, callback)</td>
    <td style="padding:15px">getDeviceLog</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNwpiDscp(callback)</td>
    <td style="padding:15px">getNwpiDscp</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/nwpiDSCP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNwpiProtocol(callback)</td>
    <td style="padding:15px">getNwpiProtocol</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/nwpiProtocol?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketFeatures(traceId, timestamp, flowId, callback)</td>
    <td style="padding:15px">getPacketFeatures</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/packetFeatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traceStart(body, callback)</td>
    <td style="padding:15px">Trace Action - Start</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/trace/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">traceStop(traceId, callback)</td>
    <td style="padding:15px">Trace Action - Stop</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/trace/stop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceFlow(traceId, timestamp, state, callback)</td>
    <td style="padding:15px">getTraceFlow</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/traceFlow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceHistory(callback)</td>
    <td style="padding:15px">getTraceHistory</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/nwpi/traceHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSession(body, callback)</td>
    <td style="padding:15px">getSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSpeedTestSession(sessionId, callback)</td>
    <td style="padding:15px">disableSpeedTestSession</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/disable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceBandwidth(circuit, deviceUUID, callback)</td>
    <td style="padding:15px">getInterfaceBandwidth</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/interface/bandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startSpeedTest(sessionId, callback)</td>
    <td style="padding:15px">startSpeedTest</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/start/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpeedTestStatus(sessionId, callback)</td>
    <td style="padding:15px">getSpeedTestStatus</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopSpeedTest(sessionId, callback)</td>
    <td style="padding:15px">stopSpeedTest</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/stop/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveSpeedTestResults(deviceUUID, sessionId, body, callback)</td>
    <td style="padding:15px">saveSpeedTestResults</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpeedTest(sessionId, logId, callback)</td>
    <td style="padding:15px">getSpeedTest</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/speed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processDeviceStatus(deviceUUID, body, callback)</td>
    <td style="padding:15px">processDeviceStatus</td>
    <td style="padding:15px">{base_path}/{version}/stream/device/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activate(callback)</td>
    <td style="padding:15px">Activate cluster to start working as primary</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClusterInfo(callback)</td>
    <td style="padding:15px">Get disaster recovery cluster info</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/clusterInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocalDC(callback)</td>
    <td style="padding:15px">Delete local data center</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/deleteLocalDataCenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDC(callback)</td>
    <td style="padding:15px">Delete data center</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/deleteRemoteDataCenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(callback)</td>
    <td style="padding:15px">Delete disaster recovery</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/deregister?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetails(callback)</td>
    <td style="padding:15px">Get disaster recovery details</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadReplicationData(token, fileName, callback)</td>
    <td style="padding:15px">Download replication data</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/download/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDisasterRecoveryStatus(callback)</td>
    <td style="padding:15px">Disaster recovery status</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/drstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistory(callback)</td>
    <td style="padding:15px">Get disaster recovery switchover history</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalHistory(callback)</td>
    <td style="padding:15px">Get disaster recovery local switchover history</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/localLatestHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocalDataCenterState(callback)</td>
    <td style="padding:15px">Get local data center details</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/localdc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseDR(callback)</td>
    <td style="padding:15px">Pause DR</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/pause?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseLocalArbitrator(callback)</td>
    <td style="padding:15px">Pause DR for Local Arbitrator</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/pauseLocalArbitrator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseLocalDCForDR(callback)</td>
    <td style="padding:15px">Pause DR for Local datacenter</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/pauseLocalDC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pauseLocalDCReplication(callback)</td>
    <td style="padding:15px">Pause DR replication for Local datacenter</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/pauseLocalReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryPauseReplication(callback)</td>
    <td style="padding:15px">Pause DR data replication</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/pausereplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(body, callback)</td>
    <td style="padding:15px">Update data centers for disaster recovery</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">register(body, callback)</td>
    <td style="padding:15px">Register data centers for disaster recovery</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteDCMembersState(callback)</td>
    <td style="padding:15px">Gets remote data center member state</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/remoteDcState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteDataCenterState(callback)</td>
    <td style="padding:15px">Get remote data center details</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/remotedc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDisasterRecoveryState(body, callback)</td>
    <td style="padding:15px">Update complete disaster recovery information to remote data center</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/remotedc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemoteDataCenterVersion(callback)</td>
    <td style="padding:15px">Get remote data center vManage version</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/remotedc/swversion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryReplicationRequest(body, callback)</td>
    <td style="padding:15px">Replication Request message sent from primary</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/requestimport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restartDataCenter(body, callback)</td>
    <td style="padding:15px">Restart data center</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/restartDataCenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDisasterRecoveryLocalReplicationSchedule(callback)</td>
    <td style="padding:15px">Get disaster recovery local replication schedule</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdrStatus(callback)</td>
    <td style="padding:15px">Get disaster recovery status</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unpauseDR(callback)</td>
    <td style="padding:15px">Unpause DR</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/unpause?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unpauseLocalArbitrator(callback)</td>
    <td style="padding:15px">Unpause DR for Local Arbitrator</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/unpauseLocalArbitrator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unpauseLocalDCForDR(callback)</td>
    <td style="padding:15px">Unpause DR for Local datacenter</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/unpauseLocalDC?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unpauseLocalDCReplication(callback)</td>
    <td style="padding:15px">Unpause DR replication for local datacenter</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/unpauseLocalReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disasterRecoveryUnPauseReplication(callback)</td>
    <td style="padding:15px">Un-Pause DR data replication</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/unpausereplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDrState(callback)</td>
    <td style="padding:15px">Update arbitrator with primary and secondary states cluster</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/updateDRConfigOnArbitrator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReplication(body, callback)</td>
    <td style="padding:15px">Update DR replication status</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/updateReplication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReachabilityInfo(body, callback)</td>
    <td style="padding:15px">Validate a list of nodes</td>
    <td style="padding:15px">{base_path}/{version}/disasterrecovery/validateNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawData21(query, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsRawData21(body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregationDataByQuery21(query, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/event/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostAggregationDataByQuery20(body, callback)</td>
    <td style="padding:15px">Get aggregated data based on input query and filters. The data can be filtered on time and other un</td>
    <td style="padding:15px">{base_path}/{version}/event/aggregation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponentsAsKeyValue(callback)</td>
    <td style="padding:15px">Retrieve components as key/value pairs</td>
    <td style="padding:15px">{base_path}/{version}/event/component/keyvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataRawDataAsCSV21(query, callback)</td>
    <td style="padding:15px">Get raw data with optional query as CSV</td>
    <td style="padding:15px">{base_path}/{version}/event/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCount23(query, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/event/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountPost23(body, callback)</td>
    <td style="padding:15px">Get response count of a query</td>
    <td style="padding:15px">{base_path}/{version}/event/doccount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableEventsFromFile(callback)</td>
    <td style="padding:15px">Set enable events from file flag</td>
    <td style="padding:15px">{base_path}/{version}/event/enable/fileprocess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatDataFields23(callback)</td>
    <td style="padding:15px">Get fields and type</td>
    <td style="padding:15px">{base_path}/{version}/event/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListenersInfo(callback)</td>
    <td style="padding:15px">Retrieve listener information</td>
    <td style="padding:15px">{base_path}/{version}/event/listeners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatBulkRawData21(query, scrollId, count, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/event/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStatBulkRawData21(scrollId, count, body, callback)</td>
    <td style="padding:15px">Get stats raw data</td>
    <td style="padding:15px">{base_path}/{version}/event/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatQueryFields23(callback)</td>
    <td style="padding:15px">Get query fields</td>
    <td style="padding:15px">{base_path}/{version}/event/query/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEventsQueryConfig(callback)</td>
    <td style="padding:15px">Create query configuration</td>
    <td style="padding:15px">{base_path}/{version}/event/query/input?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findEvents(severityLevel = 'critical', deviceId, query, callback)</td>
    <td style="padding:15px">Retrieve events</td>
    <td style="padding:15px">{base_path}/{version}/event/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeverityHistogram(deviceId, query, callback)</td>
    <td style="padding:15px">Retrieve severity histogram</td>
    <td style="padding:15px">{base_path}/{version}/event/severity/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventTypesAsKeyValue(callback)</td>
    <td style="padding:15px">Retrieve event types as key/value pairs</td>
    <td style="padding:15px">{base_path}/{version}/event/types/keyvalue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diffTwoConfigs(configId1, configId2, callback)</td>
    <td style="padding:15px">Get diff of two configs</td>
    <td style="padding:15px">{base_path}/{version}/device/history/config/diff/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVEdgePolicyDetails(callback)</td>
    <td style="padding:15px">Get vEdge policy details</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vedge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVEdgeTemplate(policyId, callback)</td>
    <td style="padding:15px">Get template</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vedge/definition/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceListByPolicy(policyId, callback)</td>
    <td style="padding:15px">Get device list by policy id</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/vedge/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyDataPrefixList(callback)</td>
    <td style="padding:15px">Get policy data prefix list</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/list/dataprefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVEdgePolicyRouteDefinition(callback)</td>
    <td style="padding:15px">Get vEdge policy definition</td>
    <td style="padding:15px">{base_path}/{version}/template/policy/definition/vedgeroute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
