# Cisco Viptela

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Viptela
Product Page: https://www.cisco.com/c/en/us/services/acquisitions/viptela.html

## Introduction
We classify Cisco Viptela into the SD-WAN/SASE domain as Cisco Viptela devices provide a Software Defined Wide Area Network (SD-WAN) solution. We also classify it into the Inventory domain because it contains an inventory of Cisco devices.

"Cisco Viptela improves connectivity for remote and hybrid workers." 
"Cisco Viptela makes the workspace safer both digitally and physically." 

The Cisco Viptela adapter can be integrated to the Itential Device Broker which will allow your Cisco devices to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Cisco Viptela adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco’s Viptela controller to offer a cloud-scalable SD-WAN solution for configuration, orchestration and monitoring of an overlay network. Viptela fits within the overall Cisco Digital Network Architecture (Cisco DNA) to complement its platform for WAN optimization, automation, virtualization and analytics.

With this adapter you have the ability to perform operations with Cisco Viptela such as:

- Configure and Manage Cisco Devices in the Cloud. 
- Add and Remove Devices to Inventory: Through IAP, a new device can be added to the network inventory so that it can be managed by Ansible Manger. A device that is no longer utilized from the network inventory can also be removed through IAP.
- Perform Pre and Post Checks: IAP allows for the ability to perform pre and post checks of a device configuration when making modifications to the device.
- Device Configuration Assurance: IAP can back up the configuration on the device in a native format that can be restored to the device if there are issues.
- Template
- Policy
- Alarm

## Additional Product Documentation
The [API documents for Cisco Viptela](https://developer.cisco.com/docs/sdwan/#!sd-wan-vmanage-v20-12)
